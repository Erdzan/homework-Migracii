<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersfakerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersfaker', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('phone_of_company');
            $table->string('title');
            $table->string('describe');
            $table->string('person_of_contact');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersfaker');
    }
}
