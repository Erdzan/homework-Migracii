<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class usersfakerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('usersfaker');
        for ($i=0; $i < 100; $i++) 
        { 
        	\DB::table('usersfaker')->insert([

        		'company_name' =>$faker->company,
        		'phone_of_company'=>$faker->phoneNumber,
        		'title'=>$faker->jobTitle,
        		'describe'=>$faker->realText($maxNbChars = 255),
        		'person_of_contact'=>$faker->name

        		]);
        }
    }
}
